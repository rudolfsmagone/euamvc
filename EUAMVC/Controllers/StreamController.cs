﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EUAMVC.Models;
using PagedList.Core;// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EUAMVC.Controllers
{
    public class StreamController : Controller
    {
        private readonly EuaDbContext _db;
        public StreamController(EuaDbContext db)
        {
            _db = db;
        }
     
        public IActionResult Stream(int page = 0)
        {
            const int PageSize = 2;
            var count = _db.Streams.Count();

            StreamList viewmodel = new StreamList();
            viewmodel.StreamHistory = _db.Streams.OrderByDescending(o => o.Id).Skip(page * PageSize).Take(PageSize).ToList();
            this.ViewBag.MaxPage = (count / PageSize) - (count % PageSize == 0 ? 1 : 0);
            this.ViewBag.Page = page;

            return View(viewmodel);
        }
    }
}
 