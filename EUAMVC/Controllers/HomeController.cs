﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EUAMVC.Models;

namespace EUAMVC.Controllers
{
    public class HomeController : Controller
    {

        private readonly EuaDbContext _db;
        public HomeController(EuaDbContext db)
        {
            _db = db;
        }
        public IActionResult Index(int page =0)
        {
             const int PageSize = 4;
            var count = _db.Articles.Count();

            IndexArticles viewmodel = new IndexArticles();
            viewmodel.importantViewModel = _db.Articles.OrderByDescending(o => o.Id).Where(o => o.ImportantNews == true).ToList();
            viewmodel.unimportantViewModel = _db.Articles.OrderByDescending(o => o.Id).Where(o => o.ImportantNews == false).Skip(page * PageSize).Take(PageSize).ToList();
            this.ViewBag.MaxPage = (count / PageSize) - (count % PageSize == 0 ? 1 : 0);
            this.ViewBag.Page = page;

            return View(viewmodel);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
