﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EUAMVC.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EUAMVC.Controllers
{
    public class ArticlesController : Controller
    {
        private readonly EuaDbContext _db;
        public ArticlesController(EuaDbContext db)
        {
            _db = db;
        }
        public IActionResult Articles(int id)
        {
           
            Articles Article = _db.Articles.Single(art => art.Id == id);
            return View(Article);
        }
        public IActionResult CreateArticles()
        {

            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CreateArticles(
    [Bind("Id,ArticleName,ArticleText,ArticleContext,ImportantNews,CreatedOn")] Articles article)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _db.Articles.Add(article);
                    _db.SaveChanges();
                    return RedirectToAction(nameof(Streams));
                }
            }
            catch (Exception /* ex */)
            {
                //Log the error (uncomment ex variable name and write a log.
                ModelState.AddModelError("", "Unable to save changes. " +
                    "Try again, and if the problem persists " +
                    "see your system administrator.");
            }
            return View(article);
        }
    }
}
