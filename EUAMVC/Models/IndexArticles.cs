﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EUAMVC.Models
{
    public class IndexArticles
    {
        public List<Articles> importantViewModel { get; set; }
        public List<Articles> unimportantViewModel { get; set; }
    }
}
