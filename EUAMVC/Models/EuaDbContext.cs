﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EUAMVC.Models
{
    public class EuaDbContext :DbContext
    {
        public EuaDbContext(DbContextOptions<EuaDbContext> options) : base(options) {

        }
    public DbSet<Articles> Articles { get; set; }
        public DbSet<Articles> CreateArticles { get; set; }
        public DbSet<Streams> Streams { get; set; }
    }
    }

 
