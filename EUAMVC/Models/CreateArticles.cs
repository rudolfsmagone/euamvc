﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EUAMVC.Models
{
    public class CreateArticles
    {
        public string ArticleName { get; set; }
        public string ArticleContext { get; set; }
        public string ArticleText { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool ImportantNews { get; set; }
    }
}
