﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EUAMVC.Models
{
    public class Streams
    {
        public int Id { get; set; }
        public string StreamHeader { get; set; }
        public string StreamLink { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
